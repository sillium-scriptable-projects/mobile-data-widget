// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: pink; icon-glyph: exchange-alt;
const DEBUG = true
const FORCE_DOWNLOAD = true

const log = DEBUG ? console.log.bind(console) : function () { };

// configure the library
const libraryInfo = {
//    url: 'https://gitlab.com/sillium-scriptable-projects/mobile-data-widget/-/raw/master/lib/createWidget.js'
    url: 'https://gitlab.com/sillium-scriptable-projects/mobile-data-widget/-/raw/master/lib/test.js'
}

// download and import library
let library = importModule(await downloadLibrary(libraryInfo))

// create the widget
const params = {
    widgetParameter: args.widgetParameter,
    debug: DEBUG,
    forceDownload: FORCE_DOWNLOAD
}
const widget = await library.createWidget(params)

// preview the widget
if (!config.runsInWidget) {
    await widget.presentSmall()
}

Script.setWidget(widget)
Script.complete()

/**
 * - creates directory for library if not existing
 * - downloads library file if not existing or forced
 * - returns relative path to library module
 * @param {{name: string, version: string, gitlabProject: string, forceDownload: bool}} library 
 */
async function downloadLibrary(library) {
    let fm = FileManager.local()

    // get name of script
    let scriptPath = module.filename
    // create path string named like the script without '.js'
    let libraryDir = scriptPath.replace(fm.fileName(scriptPath, true), fm.fileName(scriptPath, false))

    // if path exists but is no directory, delete
    if (fm.fileExists(libraryDir) && !fm.isDirectory(libraryDir)) {
        fm.remove(libraryDir)
    }
    // if not path exists, create directory
    if (!fm.fileExists(libraryDir)) {
        fm.createDirectory(libraryDir)
    }

    // extract filename from download url
    let libraryFilename = library.url.split('/').pop()

    // append filename to directory on device
    let path = fm.joinPath(libraryDir, libraryFilename)

    if (fm.fileExists(path) && FORCE_DOWNLOAD == false) {
        log("Not downloading library file '" + libraryFilename + "'")
    } else {
        log("Downloading library file '" + libraryFilename + "' from '" + library.url + "' to '" + path + "'")
        const req = new Request(library.url + '?random=' + Math.random().toString(36).substring(7))
        let libraryFile = await req.load()
        fm.write(path, libraryFile)
    }

    log(fm.fileName(scriptPath, false) + '/' + libraryFilename)

    return fm.fileName(scriptPath, false) + '/' + libraryFilename
}