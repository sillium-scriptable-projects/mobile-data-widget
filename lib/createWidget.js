//@ts-ignore

/**
 * Create the widget
 * @param {{widgetParameter: string, debug: string}} config widget configuration
 */
async function createWidget(config) {
//    const DEBUG = config.debug ? config.debug : false

    console.log("DEBUG: " + DEBUG)

//    const log = DEBUG ? console.log.bind(console) : function () { };
    log(JSON.stringify(config, null, 2))

    //=== Extract parameters ==========================================
    let param = config.widgetParameter
    let language, whatToShow, look, provider
    if (param != null && param.length > 0) {
        const parts = param.split(';')
        if (parts.length == 4) {
            language = parts[0].toLowerCase()
            whatToShow = parts[1].toLowerCase()
            look = parts[2].toLowerCase()
            provider = parts[3].toLowerCase()
        } else {
            const errorList = new ListWidget()
            errorList.addText("Please fix widget parameter configuration.")
            return errorList
        }
    } else {
        language = 'en'
        whatToShow = 'used'
        look = 'telekom'
        provider = 'telekom'
    }

    log("language: " + language)
    log("whatToShow: " + whatToShow)
    log("look: " + look)
    log("provider: " + provider)

    if (['de', 'en'].indexOf(language) == -1) {
        const errorList = new ListWidget()
        errorList.addText("Please fix widget parameter configuration 'language'.")
        return errorList
    }

    if (['used', 'available'].indexOf(whatToShow) == -1) {
        const errorList = new ListWidget()
        errorList.addText("Please fix widget parameter configuration 'whatToShow'.")
        return errorList
    }

    if (['telekom', 'congstar', 'fraenk', 'black', 'gray', 'white'].indexOf(look) == -1) {
        const errorList = new ListWidget()
        errorList.addText("Please fix widget parameter configuration 'look'.")
        return errorList
    }

    if (['telekom', 'vodafone'].indexOf(provider) == -1) {
        const errorList = new ListWidget()
        errorList.addText("Please fix widget parameter configuration 'provider'.")
        return errorList
    }

    // TODO: getProviderData

    // configure the library
    let provideribraryInfo = {
        url: 'https://gitlab.com/sillium-scriptable-projects/mobile-data-widget/-/raw/master/lib/provider/' + provider + '.js',
        forceDownload: config.forceDownload ? true : false
    }

    // download and import library
    let provider = importModule(await downloadLibrary(provideribraryInfo))
    
    let data = provider.getProviderData()

    log("data: " + JSON.stringify(data, null, 2))

    //=== Data to show =====================================
    let currentPercent = (whatToShow == 'used') ? data.percentUsed : data.percentAvailable 
    let currentVolume = (whatToShow == 'used') ? data.volumeUsed : data.volumeAvailable
    let initialVolume = data.initialVolume
    let renewsIn = formatDays(data.remainingDays)

    if (look == 'congstar') {
    } else if (look == 'fraenk') {
        showData.providerImage = await getImage('fraenk.png', config.debug)
        showData.color.foregroundActive = Color.black()
        showData.color.foregroundInactive = new Color("#CCCCCC")
        showData.color.background = Color.white()
    } else if (look == 'telekom') {
        showData.providerImage = await getImage('telekom.png', config.debug)
        showData.color.foregroundActive = Color.white()
        showData.color.foregroundInactive = new Color("#FF34A5")
        showData.color.background = new Color("#DD0273")
    } else if (look == 'white') {
        showData.providerImage = await getImage('generic_white.png', config.debug)
        showData.color.foregroundActive = Color.black()
        showData.color.foregroundInactive = new Color("#CCCCCC")
        showData.color.background = Color.white()
    } else if (look == 'gray') {
        showData.providerImage = await getImage('generic_gray.png', config.debug)
        showData.color.foregroundActive = Color.white()
        showData.color.foregroundInactive = new Color("#555555")
        showData.color.background = new Color("#2C2C2E")
    } else if (look == 'black') {
        showData.providerImage = await getImage('generic_black.png', config.debug)
        showData.color.foregroundActive = Color.white()
        showData.color.foregroundInactive = new Color("#444444")
        showData.color.background = Color.black()
    }

    //=== Create Widget =====================================
    const widget = new ListWidget()
    widget.backgroundColor = showData.color.background
    widget.setPadding(padding, padding, padding, padding)

    // === Logo =====================================
    let rowLogo = addStackTo(widget, 'h')
    rowLogo.centerAlignContent()

    const telekomIconImg = rowLogo.addImage(showData.providerImage)
    telekomIconImg.imageSize = new Size(30, 30)

    rowLogo.addSpacer(5)

    let headlineSurroundingStack = addStackTo(rowLogo, 'v')
    const headlineLabel = headlineSurroundingStack.addText(showData.label.headline)
    headlineLabel.leftAlignText()
    headlineLabel.font = Font.mediumSystemFont(12)
    headlineLabel.textColor = showData.color.foregroundActive

    widget.addSpacer()

    //=== Percent ===================================
    let rowPercentage = addStackTo(widget, 'h')
    rowPercentage.addSpacer()
    const percentText = rowPercentage.addText(showData.percent + " %")
    percentText.centerAlignText()
    percentText.font = Font.heavySystemFont(fontSizeHuge)
    percentText.textColor = (fresh == 1) ? showData.color.foregroundActive : showData.color.foregroundInactive
    rowPercentage.addSpacer()

    widget.addSpacer()

    // === Details =====================================
    let rowDetails = addStackTo(widget, 'h')

    let rowDetailsLeft = addStackTo(rowDetails, 'v')
    const detailsLabel = rowDetailsLeft.addText(showData.label.dataVolume)
    detailsLabel.leftAlignText()
    detailsLabel.font = Font.regularRoundedSystemFont(fontSizeBig)
    detailsLabel.textColor = (fresh == 1) ? showData.color.foregroundActive : showData.color.foregroundInactive

    rowDetails.addSpacer()

    let rowDetailsRight = addStackTo(rowDetails, 'v')
    const detailsText = rowDetailsRight.addText(formatVolume(showData.dataVolume, language))
    detailsText.rightAlignText()
    detailsText.font = Font.heavySystemFont(fontSizeBig)
    detailsText.textColor = (fresh == 1) ? showData.color.foregroundActive : showData.color.foregroundInactive

    // === Initial =====================================
    let rowInitial = addStackTo(widget, 'h')

    let rowInitialLeft = addStackTo(rowInitial, 'v')
    const initialLabel = rowInitialLeft.addText(showData.label.initial)
    initialLabel.leftAlignText()
    initialLabel.font = Font.regularRoundedSystemFont(fontSizeBig)
    initialLabel.textColor = (fresh == 1) ? showData.color.foregroundActive : showData.color.foregroundInactive

    rowInitial.addSpacer()

    let rowInitialRight = addStackTo(rowInitial, 'v')
    const initialText = rowInitialRight.addText(formatVolume(showData.initialVolume, language))
    initialText.rightAlignText()
    initialText.font = Font.heavySystemFont(fontSizeBig)
    initialText.textColor = (fresh == 1) ? showData.color.foregroundActive : showData.color.foregroundInactive

    // === Time period =====================================
    let rowTimePeriod = addStackTo(widget, 'h')

    let rowTimePeriodLeft = addStackTo(rowTimePeriod, 'v')
    const timePeriodLabel = rowTimePeriodLeft.addText(showData.label.renewsIn)
    timePeriodLabel.leftAlignText()
    timePeriodLabel.font = Font.regularRoundedSystemFont(fontSizeBig)
    timePeriodLabel.textColor = (fresh == 1) ? showData.color.foregroundActive : showData.color.foregroundInactive

    rowTimePeriod.addSpacer()

    let rowTimePeriodRight = addStackTo(rowTimePeriod, 'v')
    const renewsInText = rowTimePeriodRight.addText(showData.renewsIn)
    renewsInText.rightAlignText()
    renewsInText.font = Font.heavySystemFont(fontSizeBig)
    renewsInText.textColor = (fresh == 1) ? showData.color.foregroundActive : showData.color.foregroundInactive

    widget.addSpacer(2)

    return widget
}

/**
 * - downloads library file if not existing or forced
 * - returns relative path to library module
 * @param {{name: string, version: string, gitlabProject: string, forceDownload: bool}} library 
 */
async function downloadLibrary(library) {
    let fm = FileManager.local()

    // get name of script
    let scriptPath = module.filename
    // create path string with path of script
    let libraryDir = scriptPath.replace(fm.fileName(scriptPath, true), '')

    // extract filename from download url
    let libraryFilename = library.url.split('/').pop()

    // append filename to directory on device
    let path = fm.joinPath(libraryDir, libraryFilename)

    if (fm.fileExists(path) && !library.forceDownload) {
        log("Not downloading library file " + libraryFilename)
    } else {
        log("Downloading library file '" + libraryFilename + "' from '" + library.url + "' to '" + path + "'")
        const req = new Request(library.url + '?random=' + Math.random().toString(36).substring(7))
        let libraryFile = await req.load()
        fm.write(path, libraryFile)
    }

    return libraryFilename
}

module.exports = {
    createWidget
}
