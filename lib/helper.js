let fm = FileManager.local()

function getScriptDir() {
    let scriptPath = module.filename
    let scriptDir = scriptPath.replace(fm.fileName(scriptPath, true), '')
    return scriptDir
}

// get images from local filestore or download them once
async function getImage(image, forceDownload) {
    let path = fm.joinPath(getScriptDir(), image)
    if (fm.fileExists(path) && !forceDownload) {
        return fm.readImage(path)
    } else {
        // download once
        let imageUrl
        switch (image) {
            case 'telekom.png':
                imageUrl = "https://i.imgur.com/wKKfJdwt.png"
                break
            case 'congstar.png':
                imageUrl = "https://i.imgur.com/z02k5gSt.png"
                break
            case 'fraenk.png':
                imageUrl = "https://i.imgur.com/MtDumhPt.png"
                break
            case 'generic_white.png':
                imageUrl = "https://i.imgur.com/RPf5sYWt.png"
                break
            case 'generic_black.png':
                imageUrl = "https://i.imgur.com/rWz8kpFt.png"
                break
            case 'generic_gray.png':
                imageUrl = "https://i.imgur.com/bfKPtInt.png"
                break
            default:
                log(`Sorry, couldn't find ${image}.`);
        }
        let iconImage = await loadImage(imageUrl)
        fm.writeImage(path, iconImage)
        return iconImage
    }
}

// helper function to download an image from a given url
async function loadImage(imgUrl) {
    const req = new Request(imgUrl)
    return await req.loadImage()
}

function addStackTo(stack, layout) {
    const newStack = stack.addStack()
    if (DEBUG) newStack.backgroundColor = new Color(randomColor(), 1.0)
    if (layout == 'h') {
        newStack.layoutHorizontally()
    } else {
        newStack.layoutVertically()
    }
    return newStack
}

const randomColor = () => {
    let color = '#';
    for (let i = 0; i < 6; i++){
       const random = Math.random();
       const bit = (random * 16) | 0;
       color += (bit).toString(16);
    }
    return color;
 };

 function formatDataVolume(amount, language) {
    var formatterGiga = new Intl.NumberFormat(language, {
        style: 'decimal',

        minimumFractionDigits: 0,
        maximumFractionDigits: 2
    });

    let amount_str
    if (amount >= 100*1024*1024) {
        amount_str = formatterGiga.format(amount / (1024*1024*1024)) + " GB"
    } else if (amount >= 100*1024) {
        amount_str = formatterGiga.format(amount / (1024*1024)) + " MB"
    } else if (amount >= 100) {
        amount_str = formatterGiga.format(amount / 1024) + " kB"
    } else {
        amount_str = formatterGiga.format(amount) + " B"
    }
    return amount_str
}

function formatDays(days, language) {
    var formatterDays = new Intl.NumberFormat(language, {
        style: 'decimal',
        minimumFractionDigits: 0,
        maximumFractionDigits: 0
    });

    var rtfEn = new Intl.RelativeTimeFormat(language, {
        numeric: 'auto'
    });

    return rtfEn.format(formatterDays.format(data.remainingDays), 'day')
}

function widgetParameterToObj(widgetParameter) {
    let obj = {}

    if (widgetParameter == null || param.length > 0) {
        return obj
    }

    const parts = widgetParameter.split(';')
    
    // TODO:
    // foreach part in parts:
    //   keyValue = part.split('=')
    // obj[keyValue[0]] = keyValue[1]

    return obj
}

module.exports = {
    getImage,
    addStackTo,
    formatDataVolume,
    formatDays,
    widgetParameterToObj
}
