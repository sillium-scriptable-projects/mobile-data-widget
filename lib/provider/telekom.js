//@ts-ignore

let DEBUG = false

/**
 * Get data JSON from Telekom API
 * @param {{debug: string}} config widget configuration
 */
async function getProviderData(config) {
    DEBUG = config.debug ? true : false
    
    const log = DEBUG ? console.log.bind(console) : function () { };
    log(JSON.stringify(config, null, 2))

    //=== API Request ================================================
    let fm = FileManager.local()
    let scriptPath = module.filename
    let scriptDir = scriptPath.replace(fm.fileName(scriptPath, true), '')
    let path = fm.joinPath(scriptDir, "telekom-cache.json")

    const apiUrl = "https://pass.telekom.de/api/service/generic/v1/status"
    let data
    try {
        let r = new Request(apiUrl)

        // API only answers for mobile Safari
        r.headers = {
            "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Mobile/15E148 Safari/604.1"
        }
      
        try {
            // Fetch data from pass.telekom.de
            data = await r.loadJSON()

            data.remainingDays = data.remainingSeconds / (24*60*60)
            data.availableVolume = data.initialVolume - data.usedVolume
            data.availablePercentage = 100 - data.usedPercentage
        
            // Write JSON to  file
            fm.writeString(path, JSON.stringify(data, null, 2))
            data.fresh = true
        } catch (err) {
            // Read data from file
            data = JSON.parse(fm.readString(path), null)
            if (!data) {
                return {
                    'success': false,
                    'error': 'No JSON from cache'
                }
            }
            data.fresh = false
        }
        
        data.success = true

    } catch(err) {
        return {
            'success': false,
            'error': 'Error fetching JSON from https://pass.telekom.de/api/service/generic/v1/status'
        }
    }

    return data
}

module.exports = {
    getProviderData
}

